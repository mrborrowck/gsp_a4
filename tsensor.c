#include "onewire.h"
#include "tsensor.h"
#include "crc.h"
#include "timer.h"
#include "error.h"
#include "tft.h"

#include <stdio.h>

static TSENSOR_ROM dRoms[6];
static int dLen = 0;

bool readROM(DPIN pin, TSENSOR_ROM* rom) {
    errno = T_ERROR_NONE;
    
	DPIN g3 = { .gpio = GPIOG, .n = 3 };
	
    if (!owReset(pin)) {
        errno = T_ERROR_NO_SENSOR;
        return false;
    }
    
    owSendByte(pin, 0x33);
	
	writePin(g3, true);
    
    rom->familyCode = owRecvByte(pin);
    owRecvBytes(pin, rom->serialNumber, 6);
    rom->crc = owRecvByte(pin);
    
	writePin(g3, false);
	
    uint8_t calcCRC = owCRC((uint8_t*)rom, sizeof(TSENSOR_ROM) - 1);
    
    if (rom->crc != calcCRC) {
        errno = T_ERROR_CRC_MISMATCH;
        return false;
    }
	
    return true;
}

static bool discoverROMNodes(DPIN pin, uint64_t rom, int blevel, bool reset) {
	errno = T_ERROR_NONE;
	
	if (blevel == 64) {
		TSENSOR_ROM* ptrROM = &dRoms[dLen++];
		ptrROM->val = rom;
		
		if (!verifyROM(ptrROM)) {
			errno = T_ERROR_CRC_MISMATCH;
			return false;
		}
		
		return true;
	}
	
	if (reset) {
		if (!owReset(pin)) {
			errno = T_ERROR_NO_SENSOR;
			return false;
		}
		
		owSendByte(pin, 0xF0);
		
		for (int i = 0; i < blevel; ++i) {
			bool b = owRecvBit(pin);
			bool c = owRecvBit(pin);
			
			if (b == 1 && c == 1) {
				errno = T_ERROR_TRANSMISSION;
				return false;
			}
			
			owSendBit(pin, (rom >> i) & 0x01);
		}
	}
	
	bool b = owRecvBit(pin);
	bool c = owRecvBit(pin);
	
	if (b == 1 && c == 1) {
		errno = T_ERROR_TRANSMISSION;
		return false;
	}
	
	if (b == !c || !b == c) {
		owSendBit(pin, b);
		return discoverROMNodes(pin, rom | ((uint64_t)b << blevel), blevel + 1, false);
	}
	
	if (b == 0 && c == 0) {
		owSendBit(pin, 0);
		if (!discoverROMNodes(pin, rom, blevel + 1, false)) {
			return false;
		}
		return discoverROMNodes(pin, rom | (1ULL << blevel), blevel + 1, true);
	}
	
	errno = T_ERROR_TRANSMISSION;
	return false;
}

bool searchROMS(DPIN pin) {
	dLen = 0;
	return discoverROMNodes(pin, 0, 0, true);
}

bool readTemperature(DPIN pin, TSENSOR_ROM* rom, TSENSOR_TEMP* temp) {
    errno = T_ERROR_NONE;
    
    if (!owReset(pin)) {
        errno = T_ERROR_NO_SENSOR;
        return false;
    }
    
    owSendByte(pin, 0x55);
    owSendBytes(pin, (uint8_t*)rom, sizeof(TSENSOR_ROM));
    
    owSendByte(pin, 0x44);
    
    switchToPushPullHighBus(pin);
    timerDelay(1000 * 750);
    switchToOneWireBus(pin);
    
    if (!owReset(pin)) {
        errno = T_ERROR_NO_SENSOR;
        return false;
    }
    
    owSendByte(pin, 0x55);
    owSendBytes(pin, (uint8_t*)rom, sizeof(TSENSOR_ROM));
    
    owSendByte(pin, 0xBE);
    owRecvBytes(pin, (uint8_t*)temp, sizeof(TSENSOR_TEMP));
    
    uint8_t calcCRC = owCRC((uint8_t*)temp, sizeof(TSENSOR_TEMP) - 1);
    
    if (calcCRC != temp->crc) {
        errno = T_ERROR_CRC_MISMATCH;
        return false;
    }
    
    return true;
}

TSENSOR_ROM* getDROMS() {
	return dRoms;
}

int getDROMSCount() {
	return dLen;
}

bool verifyROM(TSENSOR_ROM* rom) {
	uint8_t calcCRC = owCRC((uint8_t*)rom, sizeof(TSENSOR_ROM) - 1);
    return rom->crc == calcCRC;
}
