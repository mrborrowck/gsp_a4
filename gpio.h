#ifndef GPIO_HELPERS
#define GPIO_HELPERS

#include <stdbool.h>

#include "TI_Lib.h"
#include "stm32f4xx_gpio.h"

typedef struct {
    GPIO_TypeDef* gpio;
    unsigned n;
} DPIN;

static inline void setPinMode(DPIN pin, GPIOMode_TypeDef mode) {
    GPIO_TypeDef* gpio = pin.gpio;
	gpio->MODER = (gpio->MODER & ~(3U << (pin.n * 2))) | mode << (pin.n * 2);
}

static inline void setPinDriver(DPIN pin, GPIOOType_TypeDef type) {
    GPIO_TypeDef* gpio = pin.gpio;
	gpio->OTYPER = (gpio->MODER & ~(1U << pin.n)) | type << pin.n;
}

static inline void writePin(DPIN pin, bool val) {
    GPIO_TypeDef* gpio = pin.gpio;
	if (val) {
		// BSRRL: Set bit
		gpio->BSRRL = 1U << pin.n;
	} else {
		// BSRRH: Reset bit
		gpio->BSRRH = 1U << pin.n;
	}
}

static inline bool readPin(DPIN pin) {
	return (pin.gpio->IDR & (1U << pin.n)) ? true : false;
}

#endif //!GPIO_HELPERS
