#ifndef ONEWIRE_PROT
#define ONEWIRE_PROT

#include "gpio.h"

void switchToOneWireBus(DPIN pin);
void switchToPushPullHighBus(DPIN pin);

void owSendBit(DPIN pin, bool bit);
bool owRecvBit(DPIN pin);
    
void owSendByte(DPIN pin, uint8_t val);
void owSendBytes(DPIN pin, uint8_t* ptr, int len);

uint8_t owRecvByte(DPIN pin);
void owRecvBytes(DPIN pin, uint8_t* ptr, int len);

bool owReset(DPIN pin);

#endif //!ONEWIRE_PROT
