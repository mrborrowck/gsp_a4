#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "TI_Lib.h"
#include "tft.h"
#include "timer.h"
#include "gpio.h"
#include "onewire.h"
#include "tsensor.h"
#include "error.h"

#define MOAR_SAENSOR 1
#define SERIAL_TERMINAL_PRINTF 1

static void mehrereSensoren() {
	char stringBuffer[64] = {0};
	
	DPIN g0 = { .gpio = GPIOG, .n = 0 };
    DPIN g1 = { .gpio = GPIOG, .n = 1 };
    DPIN g3 = { .gpio = GPIOG, .n = 3 };
	
    switchToOneWireBus(g0);

    setPinMode(g1, GPIO_Mode_OUT);
    setPinDriver(g1, GPIO_OType_PP);
    writePin(g1, true);
	
	setPinMode(g3, GPIO_Mode_OUT);
   
	if (!searchROMS(g0)) {
		printError();
	} else {
		TSENSOR_ROM* roms = getDROMS();
		int romCount = getDROMSCount();

		for (int i = 0; i < romCount; ++i) {
			TSENSOR_ROM* rom = roms + i;
			TFT_gotoxy(1, 8 + i);
			sprintf(stringBuffer, "0x%llx", rom->val);
			TFT_puts(stringBuffer);
#if SERIAL_TERMINAL_PRINTF
			printf("ROM %u: %s\n", i, stringBuffer);
#endif
		}
		
		while (1) {
			for (int i = 0; i < romCount; ++i) {
				TSENSOR_ROM* rom = roms + i;
				
				TSENSOR_TEMP temperature;
				memset(&temperature, 0, sizeof(TSENSOR_TEMP));
				
				if (readTemperature(g0, rom, &temperature)) {
					double tempReal = (double)temperature.temperature / 16.0f;
					TFT_gotoxy(1, 2 + i);
					sprintf(stringBuffer, "TEMP %d: %.4fC", i, tempReal);
					TFT_puts(stringBuffer);
#if SERIAL_TERMINAL_PRINTF
					printf("TEMP %u: %s\n", i, stringBuffer);
#endif
				} else {
					printError();
				}
			}	
		}
	}
}

static void singleSensor() {
	char stringBuffer[64] = {0};
	
	DPIN g0 = { .gpio = GPIOG, .n = 0 };
    DPIN g1 = { .gpio = GPIOG, .n = 1 };
    
    switchToOneWireBus(g0);

    setPinMode(g1, GPIO_Mode_OUT);
    setPinDriver(g1, GPIO_OType_PP);
    writePin(g1, true);
	
	DPIN g3 = { .gpio = GPIOG, .n = 3 };
	setPinMode(g3, GPIO_Mode_OUT);
    setPinDriver(g3, GPIO_OType_PP);
	
	TSENSOR_ROM rom;
	memset(&rom, 0, sizeof(TSENSOR_ROM));

	if (!readROM(g0, &rom)) {
		printError();
	} else {
		TSENSOR_TEMP temp;
		memset(&temp, 0, sizeof(TSENSOR_TEMP));
		
		while (1) {
			if (!readTemperature(g0, &rom, &temp)) {
				printError();
			} else {
				double tempReal = (double)temp.temperature / 16.0f;
				TFT_gotoxy(1, 2);
				sprintf(stringBuffer, "TEMP: %.4fC", tempReal);
				TFT_puts(stringBuffer);
				printf("TEMP: %.4fC\n", tempReal);
			}
		}
	}
}

int main(void) {
	Init_TI_Board();
	timerinit();
	
#if MOAR_SAENSOR
	mehrereSensoren();
#else
	singleSensor();
#endif
    
	return 0;
}
