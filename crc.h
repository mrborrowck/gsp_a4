#ifndef OW_CRC
#define OW_CRC

#include <stdint.h>

uint8_t owCRC(uint8_t* data, int len);

#endif //!OW_CRC
