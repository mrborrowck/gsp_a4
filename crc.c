#include "crc.h"

uint8_t owCRC(uint8_t* data, int len) {
    uint8_t crc = 0;
    
    // Maxim 1-Wire 8-bit CRC (poly = x^8 + x^5 + x^4 + 1)
    for (int i = 0; i < len; ++i) {
        uint8_t val = data[i];

        for (int y = 0; y < 8; ++y) {
            if ((crc ^ val) & 0x01) {
                crc = ((crc ^ 0x18u) >> 1) | 0x80u;
            } else {
                crc >>= 1;
            }

            val >>= 1;
        }
    }

    return crc;
}
