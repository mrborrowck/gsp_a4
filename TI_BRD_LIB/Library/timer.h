#ifndef __TIMER_H
#define __TIMER_H

#include <stdint.h>
#include "TI_memory_map.h"

#define CONVERT2US 84 /* Mhz */

/**
* @brief Init hardwareTimer
* @param None
* @retval None
*/
void timerinit(void);

/**
* @brief get Timer ticks
* @param None
* @retval timerTicks
*/
static inline uint32_t getTimeStamp(){
    return (TIM2->CNT);
}

/**
* @brief resets the Timer
* @param None
* @retval None
*/
static inline void resetTimer(void) {
    TIM2->CNT = 0;
}

/**
* @brief delays the execution
* @param usec - micro seconds to wait
* @retval None
*/
static inline void timerDelay(int usec) {
	// Timer f = 84MHz
	// => 84 ticks = 1us
	int ticks = 84 * usec;
	
	resetTimer();
	while (getTimeStamp() <= ticks) {}
}

#endif
