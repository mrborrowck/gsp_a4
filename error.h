#ifndef TEMP_ERROR
#define TEMP_ERROR

#include <errno.h>

typedef enum {
    T_ERROR_NONE = 0,
    T_ERROR_NO_SENSOR,
    T_ERROR_CRC_MISMATCH,
	T_ERROR_TRANSMISSION,
} ErrorCodes;

void printError(void);

#endif //!TEMP_ERROR
