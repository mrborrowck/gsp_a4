#include "onewire.h"
#include "timer.h"

typedef enum {
	BUS_LOW,
	BUS_RELEASE
} OW_BUS_STATE;

void switchToOneWireBus(DPIN pin) {
    setPinMode(pin, GPIO_Mode_OUT);
    setPinDriver(pin, GPIO_OType_OD);
}

void switchToPushPullHighBus(DPIN pin) {
    setPinMode(pin, GPIO_Mode_OUT);
    setPinDriver(pin, GPIO_OType_PP);
    writePin(pin, true);
}

static inline void owSetBus(DPIN pin, OW_BUS_STATE state) {
	writePin(pin, (state == BUS_LOW) ? false : true);
}

void owSendBit(DPIN pin, bool bit) {
	owSetBus(pin, BUS_LOW);
	timerDelay(bit ? 6 : 60);
	writePin(pin, BUS_RELEASE);
	timerDelay(bit ? 64 : 10);
}

bool owRecvBit(DPIN pin) {
	bool recv;
	
	owSetBus(pin, BUS_LOW);
	timerDelay(6);
	owSetBus(pin, BUS_RELEASE);
	timerDelay(9);
	recv = readPin(pin);
	timerDelay(55);
	
	return recv;
}

bool owReset(DPIN pin) {
	bool reset;
	
	owSetBus(pin, BUS_LOW);
	timerDelay(480);
	owSetBus(pin, BUS_RELEASE);
	timerDelay(70);
	reset = readPin(pin);
	timerDelay(410);
	
	return !reset;
}

void owSendByte(DPIN pin, uint8_t val) {
	int bits = 8;
	while (bits--) {
		owSendBit(pin, val & 0x01);
		val >>= 1;
	}
}

void owSendBytes(DPIN pin, uint8_t* ptr, int len) {
    for (int i = 0; i < len; ++i) {
        owSendByte(pin, ptr[i]);
    } 
}

uint8_t owRecvByte(DPIN pin) {
	uint8_t val = 0;
    
    for (int i = 0; i < 8; ++i) {
        val |= (owRecvBit(pin) << i);
    }

	return val;
}

void owRecvBytes(DPIN pin, uint8_t* ptr, int len) {
    for (int i = 0; i < len; ++i) {
        ptr[i] = owRecvByte(pin);
    } 
}
