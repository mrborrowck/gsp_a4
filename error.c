#include "error.h"
#include "tft.h"

#include <stddef.h>

void printError() {
    char* errorMsg = NULL;
    
    switch (errno) {
        case T_ERROR_CRC_MISMATCH:
            errorMsg = "Communication error (CRC mismatch)";
            break;
        case T_ERROR_NO_SENSOR:
            errorMsg = "No sensor detected";
            break;
		case T_ERROR_TRANSMISSION:
			errorMsg = "Transmission error";
            break;
        case T_ERROR_NONE:
        default:
            return;
    }
    
    TFT_puts(errorMsg);
    TFT_newline();
}
