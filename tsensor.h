#ifndef TSENSOR
#define TSENSOR

#pragma anon_unions

#include <stdint.h>
#include <stdbool.h>

typedef union {
	struct __attribute__((__packed__)) {
		uint8_t familyCode;
		uint8_t serialNumber[6];
		uint8_t crc;
	};
	uint64_t val;
} TSENSOR_ROM;

typedef struct __attribute__((__packed__)) {
    int16_t temperature;
    uint8_t scratch[6];
    uint8_t crc;
} TSENSOR_TEMP;

bool readROM(DPIN pin, TSENSOR_ROM* rom);

bool searchROMS(DPIN pin);

bool readTemperature(DPIN pin, TSENSOR_ROM* rom, TSENSOR_TEMP* temp);

TSENSOR_ROM* getDROMS(void);
int getDROMSCount(void);

bool verifyROM(TSENSOR_ROM* rom);

#endif //!TSENSOR
